import React from 'react';

export const DrawerContext = React.createContext({
  isDrawerOpen: false,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  setIsDrawerOpen: (newIsDrawerOpen: boolean) => {},
});
