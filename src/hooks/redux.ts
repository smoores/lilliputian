import { useSelector, shallowEqual } from 'react-redux';
import { AppState } from '../store';

export function useShallowEqualSelector<Return>(
  selector: (state: AppState) => Return
): Return {
  return useSelector(selector, shallowEqual);
}
