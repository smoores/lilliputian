import { useEffect, useCallback, useContext } from 'react';
import { goBack } from 'connected-react-router';
import { DrawerContext } from '../contexts/DrawerContext';
import { useDispatch } from 'react-redux';

export const useDrawer = (isOpen: boolean) => {
  const dispatch = useDispatch();
  const { setIsDrawerOpen } = useContext(DrawerContext);
  const onClose = useCallback(() => dispatch(goBack()), [dispatch]);
  useEffect(() => {
    setIsDrawerOpen(isOpen);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);
  return { onClose };
};
