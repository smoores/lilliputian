import { EntityCache } from '..';
import {
  UserActionTypes,
  HomePageLoadedAction,
  FeedPageLoadedAction,
} from '../actions/userActions';
import {
  ServerActionTypes,
  FeedTreeLoadedAction,
  FeedTreeLoadCanceledAction,
  HeadlinesLoadedAction,
} from '../actions/serverActions';

export interface Feed {
  id: string;
  cat_id: number;
  name: string;
  feed_url: string;
  unread: number;
  has_icon: boolean;
  icon: string;
  last_updated: number;
  order_id: number;
  headlines?: string[];
}

export type FeedsState = EntityCache<Feed>;

type FeedsAction =
  | FeedPageLoadedAction
  | HomePageLoadedAction
  | FeedTreeLoadedAction
  | HeadlinesLoadedAction
  | FeedTreeLoadCanceledAction;

export function feedsReducer(
  state: FeedsState = { isLoading: false, entities: {} },
  action: FeedsAction
): FeedsState {
  switch (action.type) {
    case UserActionTypes.FEED_PAGE_LOADED: {
      const { feedId } = action.payload;
      if (!state.entities[feedId]) {
        return { ...state, isLoading: true };
      }
      return state;
    }
    case ServerActionTypes.FEED_TREE_LOAD_CANCELED: {
      return { ...state, isLoading: false };
    }
    case UserActionTypes.HOME_PAGE_LOADED: {
      return { ...state, isLoading: true };
    }
    case ServerActionTypes.HEADLINES_LOADED: {
      const { feedId, isCat, headlines } = action.payload;
      if (!isCat) {
        return {
          ...state,
          entities: {
            ...state.entities,
            [feedId]: {
              ...state.entities[feedId],
              headlines: headlines.map((headline) => headline.id),
            },
          },
        };
      }
      // If the headlines are from a category, update all of headlines lists
      // for any feeds in that category
      const updatedFeedIds = new Set<string>();
      headlines.forEach((headline) => updatedFeedIds.add(headline.feed_id));
      return {
        ...state,
        entities: {
          ...Array.from(updatedFeedIds).reduce(
            (acc, updatedFeedId) => ({
              ...acc,
              [updatedFeedId]: {
                ...acc[updatedFeedId],
                headlines: headlines
                  .filter((headline) => headline.feed_id === updatedFeedId)
                  .map((headline) => headline.id),
              },
            }),
            state.entities
          ),
        },
      };
    }
    case ServerActionTypes.FEED_TREE_LOADED: {
      const { feedTree } = action.payload;
      const entities = feedTree.reduce(
        (acc: { [id: string]: Feed }, category) =>
          category.items.reduce((acc: { [id: string]: Feed }, feed) => {
            acc[feed.bare_id] = {
              ...state.entities[feed.bare_id],
              ...feed,
              id: feed.bare_id,
            };
            return acc;
          }, acc),
        {}
      );
      return { isLoading: false, entities };
    }
    default: {
      return state;
    }
  }
}
