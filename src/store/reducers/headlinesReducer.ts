import { EntityCache } from '../';
import { UserActionTypes, FeedPageLoadedAction } from '../actions/userActions';
import {
  ServerActionTypes,
  HeadlinesLoadedAction,
  HeadlinesLoadCanceledAction,
  ArticleMarkedReadAction,
} from '../actions/serverActions';

export interface Headline {
  id: string;
  guid: string;
  unread: boolean;
  marked: boolean;
  published: boolean;
  updated: number;
  is_updated: boolean;
  title: string;
  link: string;
  feed_id: string;
  tags: string[];
  labels: string[];
  feed_title: string;
  author: string;
  lang: string;
  content: string;
}

export type HeadlinesState = EntityCache<Headline>;

type HeadlinesAction =
  | FeedPageLoadedAction
  | HeadlinesLoadedAction
  | HeadlinesLoadCanceledAction
  | ArticleMarkedReadAction;

export function headlinesReducer(
  state: HeadlinesState = { isLoading: false, entities: {} },
  action: HeadlinesAction
): HeadlinesState {
  switch (action.type) {
    case UserActionTypes.FEED_PAGE_LOADED: {
      return { ...state, isLoading: true };
    }
    case ServerActionTypes.HEADLINES_LOADED: {
      const { headlines } = action.payload;
      const entities = headlines.reduce((acc, headline) => {
        acc[headline.id] = headline;
        return acc;
      }, state.entities);

      return { isLoading: false, entities };
    }
    case ServerActionTypes.HEADLINES_LOAD_CANCELED: {
      return { ...state, isLoading: false };
    }
    case ServerActionTypes.ARTICLE_MARKED_READ: {
      const { articleId } = action.payload;
      if (!state.entities[articleId]) {
        return state;
      }
      return {
        ...state,
        entities: {
          ...state.entities,
          [articleId]: {
            ...state.entities[articleId],
            unread: false,
          },
        },
      };
    }
    default:
      return state;
  }
}
