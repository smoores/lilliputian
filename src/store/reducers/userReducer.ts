import { LoginClickedAction, UserActionTypes } from '../actions/userActions';
import {
  ServerActionTypes,
  SessionStartedAction,
  SessionEndedAction,
} from '../actions/serverActions';

type UserAction =
  | LoginClickedAction
  | SessionStartedAction
  | SessionEndedAction;

export interface UserState {
  isLoading: boolean;
  userName?: string;
  sid?: string;
}

export function userReducer(
  state: UserState = { isLoading: false },
  action: UserAction
): UserState {
  switch (action.type) {
    case UserActionTypes.LOGIN_CLICKED: {
      return { isLoading: true, userName: action.payload.userName };
    }
    case ServerActionTypes.SESSION_STARTED: {
      return { ...state, isLoading: false, sid: action.payload.sid };
    }
    case ServerActionTypes.SESSION_ENDED: {
      return { isLoading: false };
    }
    default: {
      return state;
    }
  }
}
