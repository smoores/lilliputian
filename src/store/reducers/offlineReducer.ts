import { Action, isServerBoundActionMeta } from '../actions';
import { NetworkActionTypes } from '../actions/networkActions';

export enum NetworkStatus {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
}

export interface OfflineState {
  networkStatus: NetworkStatus;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  queuedActions: Action<any, any, any>[];
}

export function offlineReducer(
  state: OfflineState = {
    networkStatus: navigator.onLine
      ? NetworkStatus.ONLINE
      : NetworkStatus.OFFLINE,
    queuedActions: [],
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: Action<any, any, any>
) {
  switch (action.type) {
    case NetworkActionTypes.NETWORK_CAME_ONLINE: {
      return { ...state, networkStatus: NetworkStatus.ONLINE };
    }
    case NetworkActionTypes.NETWORK_WENT_OFFLINE: {
      return { ...state, networkStatus: NetworkStatus.OFFLINE };
    }
    default: {
      if (
        state.networkStatus === NetworkStatus.OFFLINE &&
        action.meta &&
        isServerBoundActionMeta(action.meta) &&
        action.meta.shouldBeQueued
      ) {
        return { ...state, queuedActions: [...state.queuedActions, action] };
      }
      if (state.networkStatus === NetworkStatus.ONLINE) {
        return {
          ...state,
          queuedActions: state.queuedActions.filter(
            (queuedAction) => queuedAction !== action
          ),
        };
      }
      return state;
    }
  }
}
