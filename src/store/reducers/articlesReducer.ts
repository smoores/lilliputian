import {
  ServerActionTypes,
  ArticlesLoadedAction,
  ArticleMarkedReadAction,
} from '../actions/serverActions';
import { EntityCache } from '../';

export interface Attachment {
  id: string;
  content_url: string;
  content_type: string;
  post_id: string;
  title: string;
  duration: string;
  width: string;
  height: string;
}

export interface Article {
  id: string;
  guid: string;
  title: string;
  link: string;
  labels: string[];
  unread: boolean;
  marked: boolean;
  published: boolean;
  comments: string;
  author: string;
  updated: number;
  feed_id: string;
  attachments: Attachment[];
  score: number;
  feed_title: string;
  lang: string;
  content: string;
}

export interface ArticleFuture {
  entity: Article;
  isLoading: false;
}

export type ArticlesState = EntityCache<ArticleFuture>;

export type ArticleAction = ArticlesLoadedAction | ArticleMarkedReadAction;

export function articlesReducer(
  state: ArticlesState = { isLoading: false, entities: {} },
  action: ArticleAction
): ArticlesState {
  switch (action.type) {
    case ServerActionTypes.ARTICLES_LOADED: {
      const articleFutures = action.payload.articles.reduce((acc, article) => {
        acc[article.id] = {
          entity: article,
          isLoading: false,
        };
        return acc;
      }, {} as { [id: string]: ArticleFuture });
      return { ...state, entities: { ...state.entities, ...articleFutures } };
    }
    case ServerActionTypes.ARTICLE_MARKED_READ: {
      const { articleId } = action.payload;
      if (!state.entities[articleId]) {
        return state;
      }
      return {
        ...state,
        entities: {
          ...state.entities,
          [articleId]: {
            ...state.entities[articleId],
            entity: {
              ...state.entities[articleId].entity,
              unread: false,
            },
          },
        },
      };
    }
    default: {
      return state;
    }
  }
}
