import { EntityCache } from '../';
import { UserActionTypes, HomePageLoadedAction } from '../actions/userActions';
import {
  ServerActionTypes,
  FeedTreeLoadedAction,
  FeedTreeLoadCanceledAction,
  HeadlinesLoadedAction,
} from '../actions/serverActions';

export interface Category {
  id: string;
  feeds: string[];
  name: string;
  unread: number;
  order_id: number;
  headlines?: string[];
}

export type CategoriesState = EntityCache<Category>;

type CategoriesAction =
  | HomePageLoadedAction
  | FeedTreeLoadedAction
  | HeadlinesLoadedAction
  | FeedTreeLoadCanceledAction;

export function categoriesReducer(
  state: CategoriesState = { isLoading: false, entities: {} },
  action: CategoriesAction
): CategoriesState {
  switch (action.type) {
    case UserActionTypes.HOME_PAGE_LOADED: {
      return { ...state, isLoading: true };
    }
    case ServerActionTypes.HEADLINES_LOADED: {
      const { feedId, isCat, headlines } = action.payload;
      if (!isCat) {
        return state;
      }
      return {
        ...state,
        entities: {
          ...state.entities,
          [feedId]: {
            ...state.entities[feedId],
            headlines: headlines.map((headline) => headline.id),
          },
        },
      };
    }
    case ServerActionTypes.FEED_TREE_LOADED: {
      const { feedTree } = action.payload;
      const entities = feedTree.reduce(
        (acc: { [id: string]: Category }, category) => {
          // eslint-disable-next-line @typescript-eslint/camelcase
          const { bare_id, items, ...newCategory } = {
            ...category,
            id: category.bare_id,
            feeds: category.items.map((feed) => feed.bare_id),
          };
          acc[newCategory.id] = { ...state.entities[bare_id], ...newCategory };
          return acc;
        },
        {}
      );
      return { isLoading: false, entities };
    }
    case ServerActionTypes.FEED_TREE_LOAD_CANCELED: {
      return { ...state, isLoading: false };
    }
  }
  return state;
}
