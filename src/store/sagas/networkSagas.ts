import { takeEvery, put, select } from 'redux-saga/effects';
import { NetworkActionTypes } from '../actions/networkActions';

export function* retryQueuedActions() {
  const queuedActions = yield select((state) => state.offline.queuedActions);
  for (let i = 0; i < queuedActions.length; i++) {
    yield put(queuedActions[i]);
  }
}

export function* watchNetworkActions() {
  yield takeEvery(NetworkActionTypes.NETWORK_CAME_ONLINE, retryQueuedActions);
}
