import { takeEvery, all, put, call } from 'redux-saga/effects';
import localForage from 'localforage';
import { UserActionTypes, LoginClickedAction } from '../actions/userActions';
import { sessionStarted, ServerActionTypes } from '../actions/serverActions';
import { callIfOnline } from '.';
import { LOCATION_CHANGE } from 'connected-react-router';
import { AnyAction } from 'redux';

export async function sendLoginRequest(userName: string, password: string) {
  const body = JSON.stringify({ op: 'login', user: userName, password });
  const loginResult = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return loginResult.json();
}

export function* fetchSessionId(action: LoginClickedAction) {
  const { userName, password } = action.payload;
  const jsonBody = yield call(sendLoginRequest, userName, password);
  if (jsonBody && !jsonBody.content.error) {
    localForage.setItem('sid', jsonBody.content.session_id);
    yield put(sessionStarted(jsonBody.content.session_id));
  } else {
    localForage.setItem('sid', undefined);
  }
}

export async function sendIsLoggedInRequest(sid: string) {
  const body = JSON.stringify({ op: 'isLoggedIn', sid });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return await result.json();
}

export function* autoLogin() {
  const sid = (yield call(
    localForage.getItem.bind(localForage),
    'sid'
  )) as string;
  if (sid) {
    const jsonBody = yield call(sendIsLoggedInRequest, sid);
    if (jsonBody && !jsonBody.content.error && jsonBody.content.status) {
      yield put(sessionStarted(sid));
    } else {
      localForage.setItem('sid', undefined);
    }
  }
}

export function* watchLoginActions() {
  yield all([
    takeEvery(UserActionTypes.LOGIN_CLICKED, callIfOnline, fetchSessionId),
    takeEvery(
      [
        ServerActionTypes.SESSION_ENDED,
        (action: AnyAction) =>
          action.type === LOCATION_CHANGE &&
          action.payload.location.pathname === '/login',
      ],
      callIfOnline,
      autoLogin
    ),
  ]);
}
