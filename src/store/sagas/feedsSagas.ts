import { takeEvery, all, call, put, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {
  UserActionTypes,
  FeedPageLoadedAction,
  ArticleDrawerOpenedAction,
  SubscribeToFeedSubmittedAction,
  homePageLoaded,
} from '../actions/userActions';
import {
  ServerActionTypes,
  HeadlinesLoadedAction,
  articleMarkedRead,
  articlesLoaded,
  articlesLoadCanceled,
  feedTreeLoaded,
  feedTreeLoadCanceled,
  headlinesLoaded,
  headlinesLoadCanceled,
  sessionEnded,
} from '../actions/serverActions';

import { Headline } from '../reducers/headlinesReducer';
import { callIfOnline } from './';
import { AppState, CachedEntities } from '..';
import { Feed } from '../reducers/feedsReducer';
import { Category } from '../reducers/categoriesReducer';

export async function sendGetFeedsRequest(sid: string) {
  const body = JSON.stringify({ op: 'getFeedTree', sid });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return await result.json();
}

export function* fetchFeeds() {
  const sid = yield select((state) => state.user.sid);
  const jsonBody = yield call(sendGetFeedsRequest, sid);
  if (jsonBody && !jsonBody.content.error) {
    yield put(feedTreeLoaded(jsonBody.content.categories.items));
  } else {
    if (jsonBody.content.error === 'NOT_LOGGED_IN') {
      yield put(sessionEnded());
    }
    yield put(feedTreeLoadCanceled());
  }
}

export async function sendGetHeadlinesRequest(
  feedId: string,
  isCat: boolean,
  sid: string
) {
  const body = JSON.stringify({
    op: 'getHeadlines',
    // eslint-disable-next-line @typescript-eslint/camelcase
    feed_id: feedId,
    // eslint-disable-next-line @typescript-eslint/camelcase
    is_cat: isCat,
    // eslint-disable-next-line @typescript-eslint/camelcase
    order_by: 'feed_dates',
    sid,
  });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return await result.json();
}

export function* fetchHeadlines(action: FeedPageLoadedAction) {
  const sid = yield select((state) => state.user.sid);
  const { feedId, isCat } = action.payload;
  const jsonBody = yield call(sendGetHeadlinesRequest, feedId, isCat, sid);
  if (jsonBody && !jsonBody.content.error) {
    yield put(headlinesLoaded(feedId, isCat, jsonBody.content));
  } else {
    if (jsonBody.content.error === 'NOT_LOGGED_IN') {
      yield put(sessionEnded());
    }
    yield put(headlinesLoadCanceled());
  }
}

export function* checkAndFetchFeeds(action: FeedPageLoadedAction) {
  const feeds = (yield select(
    (state: AppState) => state.feeds.entities
  )) as CachedEntities<Feed>;
  const categories = (yield select(
    (state: AppState) => state.categories.entities
  )) as CachedEntities<Category>;
  const { feedId, isCat } = action.payload;
  if (isCat ? !categories[feedId] : !feeds[feedId]) {
    yield call(fetchFeeds);
  }
  const updatedFeeds = (yield select(
    (state) => state.feeds.entities
  )) as CachedEntities<Feed>;
  const updatedCategories = (yield select(
    (state: AppState) => state.categories.entities
  )) as CachedEntities<Category>;
  if (isCat ? !updatedCategories[feedId] : !updatedFeeds[feedId]) {
    yield put(push('/'));
  }
}

export async function sendGetArticlesRequest(articleIds: string, sid: string) {
  const body = JSON.stringify({
    op: 'getArticle',
    // eslint-disable-next-line @typescript-eslint/camelcase
    article_id: articleIds,
    sid,
  });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return await result.json();
}

export async function sendSubscribeToFeedRequest(
  feedUrl: string,
  categoryId: string,
  sid: string
) {
  const body = JSON.stringify({
    op: 'subscribeToFeed',
    // eslint-disable-next-line @typescript-eslint/camelcase
    feed_url: feedUrl,
    // eslint-disable-next-line @typescript-eslint/camelcase
    category_id: categoryId,
    sid,
  });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    { method: 'post', body }
  );
  return await result.json();
}

export function* fetchArticles(action: HeadlinesLoadedAction) {
  const { headlines } = action.payload;
  const batches = headlines.reduce(
    (acc: string[][], headline: Headline) => {
      const lastBatch = acc[acc.length - 1];
      if (lastBatch.length < 10) {
        lastBatch.push(headline.id);
      } else {
        acc.push([headline.id]);
      }
      return acc;
    },
    [[]]
  );
  const sid = yield select((state) => state.user.sid);
  for (let i = 0; i < batches.length; i++) {
    const jsonBody = yield call(
      sendGetArticlesRequest,
      batches[i].join(','),
      sid
    );
    if (jsonBody && !jsonBody.content.error) {
      yield put(articlesLoaded(jsonBody.content));
    } else {
      if (jsonBody.content.error === 'NOT_LOGGED_IN') {
        yield put(sessionEnded());
      }
      yield put(articlesLoadCanceled());
    }
  }
}

export async function sendArticleReadRequest(articleId: string, sid: string) {
  const body = JSON.stringify({
    op: 'updateArticle',
    // eslint-disable-next-line @typescript-eslint/camelcase
    article_ids: articleId,
    mode: 0,
    field: 2,
    sid,
  });
  const result = await fetch(
    `${process.env.REACT_APP_TTRSS_ENDPOINT}/api/index.php`,
    {
      method: 'post',
      body,
    }
  );
  return await result.json();
}

export function* updateArticleRead(action: ArticleDrawerOpenedAction) {
  const { articleId } = action.payload;
  const sid = yield select((state) => state.user.sid);
  const jsonBody = yield call(sendArticleReadRequest, articleId, sid);
  if (
    jsonBody &&
    jsonBody.content.status === 'OK' &&
    jsonBody.content.updated > 0
  ) {
    yield put(articleMarkedRead(articleId));
  }
  if (jsonBody && jsonBody.content.error === 'NOT_LOGGED_IN') {
    yield put(sessionEnded());
  }
}

export function* subscribeToFeed(action: SubscribeToFeedSubmittedAction) {
  const { feedUrl, categoryId } = action.payload;
  const sid = yield select((state) => state.user.sid);
  const jsonBody = yield call(
    sendSubscribeToFeedRequest,
    feedUrl,
    categoryId,
    sid
  );
  if (jsonBody && jsonBody.content.status.code === 1) {
    yield put(push('/'));
    yield put(homePageLoaded());
  }
  if (jsonBody && jsonBody.content.error === 'NOT_LOGGED_IN') {
    yield put(sessionEnded());
  }
}

export function* watchFeedActions() {
  yield all([
    takeEvery(ServerActionTypes.HEADLINES_LOADED, callIfOnline, fetchArticles),
    takeEvery(UserActionTypes.HOME_PAGE_LOADED, callIfOnline, fetchFeeds),
    takeEvery(
      UserActionTypes.FEED_PAGE_LOADED,
      callIfOnline,
      checkAndFetchFeeds
    ),
    takeEvery(UserActionTypes.FEED_PAGE_LOADED, callIfOnline, fetchHeadlines),
    takeEvery(
      UserActionTypes.ARTICLE_DRAWER_OPENED,
      callIfOnline,
      updateArticleRead
    ),
    takeEvery(
      UserActionTypes.SUBSCRIBE_TO_FEED_SUBMITTED,
      callIfOnline,
      subscribeToFeed
    ),
  ]);
}
