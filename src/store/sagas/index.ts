import { call, select } from 'redux-saga/effects';
import { NetworkStatus } from '../reducers/offlineReducer';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function* callIfOnline(...args: any[]) {
  const networkStatus = yield select((state) => state.offline.networkStatus);
  if (networkStatus === NetworkStatus.ONLINE) {
    const [worker, ...workerArgs] = args;
    try {
      yield call(worker, ...workerArgs);
    } catch (e) {
      console.warn(e);
    }
  }
}
