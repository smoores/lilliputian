import { Action, ServerBoundActionMeta } from '.';

export enum UserActionTypes {
  LOGIN_CLICKED = 'LOGIN_CLICKED',
  AUTO_LOGGED_IN = 'AUTO_LOGGED_IN',
  HOME_PAGE_LOADED = 'HOME_PAGE_LOADED',
  FEED_PAGE_LOADED = 'FEED_PAGE_LOADED',
  ARTICLE_DRAWER_OPENED = 'ARTICLE_DRAWER_OPENED',
  SUBSCRIBE_TO_FEED_SUBMITTED = 'SUBSCRIBE_TO_FEED_SUBMITTED',
}

export type LoginClickedAction = Action<
  UserActionTypes.LOGIN_CLICKED,
  { userName: string; password: string },
  ServerBoundActionMeta
>;
export const loginClicked = (
  userName: string,
  password: string
): LoginClickedAction => ({
  type: UserActionTypes.LOGIN_CLICKED,
  payload: { userName, password },
});

export type AutoLoggedInAction = Action<
  UserActionTypes.AUTO_LOGGED_IN,
  { userName: string; password: string },
  ServerBoundActionMeta
>;
export const autoLoggedIn = (
  userName: string,
  password: string
): LoginClickedAction => ({
  type: UserActionTypes.LOGIN_CLICKED,
  payload: { userName, password },
});

export type HomePageLoadedAction = Action<
  UserActionTypes.HOME_PAGE_LOADED,
  undefined,
  ServerBoundActionMeta
>;
export const homePageLoaded = (): HomePageLoadedAction => ({
  type: UserActionTypes.HOME_PAGE_LOADED,
  payload: undefined,
  meta: { shouldBeQueued: true },
});

export type FeedPageLoadedAction = Action<
  UserActionTypes.FEED_PAGE_LOADED,
  { feedId: string; isCat: boolean },
  ServerBoundActionMeta
>;
export const feedPageLoaded = (
  feedId: string,
  isCat: boolean
): FeedPageLoadedAction => ({
  type: UserActionTypes.FEED_PAGE_LOADED,
  payload: { feedId, isCat },
  meta: { shouldBeQueued: true },
});

export type ArticleDrawerOpenedAction = Action<
  UserActionTypes.ARTICLE_DRAWER_OPENED,
  { articleId: string },
  ServerBoundActionMeta
>;
export const articleDrawerOpened = (
  articleId: string
): ArticleDrawerOpenedAction => ({
  type: UserActionTypes.ARTICLE_DRAWER_OPENED,
  payload: { articleId },
  meta: { shouldBeQueued: true },
});

export type SubscribeToFeedSubmittedAction = Action<
  UserActionTypes.SUBSCRIBE_TO_FEED_SUBMITTED,
  { feedUrl: string; categoryId: string }
>;
export const subscribeToFeedSubmitted = (
  feedUrl: string,
  categoryId: string
): SubscribeToFeedSubmittedAction => ({
  type: UserActionTypes.SUBSCRIBE_TO_FEED_SUBMITTED,
  payload: { feedUrl, categoryId },
});
