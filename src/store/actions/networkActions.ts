import { Action, NetworkActionMeta } from './';

export enum NetworkActionTypes {
  NETWORK_WENT_OFFLINE = 'NETWORK_WENT_OFFLINE',
  NETWORK_CAME_ONLINE = 'NETWORK_CAME_ONLINE',
}

export type NetworkWentOfflineAction = Action<
  NetworkActionTypes.NETWORK_WENT_OFFLINE,
  NetworkActionMeta
>;
export const networkWentOffline = () => ({
  type: NetworkActionTypes.NETWORK_WENT_OFFLINE,
});

export type NetworkCameOnlineAction = Action<
  NetworkActionTypes.NETWORK_CAME_ONLINE,
  NetworkActionMeta
>;
export const networkCameOnline = () => ({
  type: NetworkActionTypes.NETWORK_CAME_ONLINE,
});
