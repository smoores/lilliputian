import { Action } from './';
import { Headline } from '../reducers/headlinesReducer';
import { Feed } from '../reducers/feedsReducer';
import { Category } from '../reducers/categoriesReducer';
import { Article } from '../reducers/articlesReducer';

export interface ServerFeed extends Feed {
  bare_id: string;
}

export interface ServerCategory extends Category {
  items: ServerFeed[];
  bare_id: string;
}

export enum ServerActionTypes {
  SESSION_STARTED = 'SESSION_STARTED',
  SESSION_ENDED = 'SESSION_ENDED',
  FEED_TREE_LOADED = 'FEED_TREE_LOADED',
  FEED_TREE_LOAD_CANCELED = 'FEED_TREE_LOAD_CANCELED',
  HEADLINES_LOADED = 'HEADLINES_LOADED',
  HEADLINES_LOAD_CANCELED = 'HEADLINES_LOAD_CANCELED',
  ARTICLES_LOADED = 'ARTICLES_LOADED',
  ARTICLES_LOAD_CANCELED = 'ARTICLES_LOAD_CANCELED',
  ARTICLE_MARKED_READ = 'ARTICLE_MARKED_READ',
}

export type SessionStartedAction = Action<
  ServerActionTypes.SESSION_STARTED,
  { sid: string }
>;
export const sessionStarted = (sid: string): SessionStartedAction => ({
  type: ServerActionTypes.SESSION_STARTED,
  payload: { sid },
});

export type SessionEndedAction = Action<ServerActionTypes.SESSION_ENDED>;
export const sessionEnded = (): SessionEndedAction => ({
  type: ServerActionTypes.SESSION_ENDED,
  payload: undefined,
});

export type FeedTreeLoadedAction = Action<
  ServerActionTypes.FEED_TREE_LOADED,
  { feedTree: ServerCategory[] }
>;
export const feedTreeLoaded = (
  feedTree: ServerCategory[]
): FeedTreeLoadedAction => ({
  type: ServerActionTypes.FEED_TREE_LOADED,
  payload: { feedTree },
});

export type FeedTreeLoadCanceledAction = Action<
  ServerActionTypes.FEED_TREE_LOAD_CANCELED
>;
export const feedTreeLoadCanceled = (): FeedTreeLoadCanceledAction => ({
  type: ServerActionTypes.FEED_TREE_LOAD_CANCELED,
  payload: undefined,
});

export type HeadlinesLoadedAction = Action<
  ServerActionTypes.HEADLINES_LOADED,
  { feedId: string; isCat: boolean; headlines: Headline[] }
>;
export const headlinesLoaded = (
  feedId: string,
  isCat: boolean,
  headlines: Headline[]
): HeadlinesLoadedAction => ({
  type: ServerActionTypes.HEADLINES_LOADED,
  payload: { feedId, isCat, headlines },
});

export type HeadlinesLoadCanceledAction = Action<
  ServerActionTypes.HEADLINES_LOAD_CANCELED
>;
export const headlinesLoadCanceled = (): HeadlinesLoadCanceledAction => ({
  type: ServerActionTypes.HEADLINES_LOAD_CANCELED,
  payload: undefined,
});

export type ArticlesLoadedAction = Action<
  ServerActionTypes.ARTICLES_LOADED,
  { articles: Article[] }
>;
export const articlesLoaded = (articles: Article[]) => ({
  type: ServerActionTypes.ARTICLES_LOADED,
  payload: { articles },
});

export type ArticlesLoadCanceledAction = Action<
  ServerActionTypes.ARTICLES_LOAD_CANCELED
>;
export const articlesLoadCanceled = (): ArticlesLoadCanceledAction => ({
  type: ServerActionTypes.ARTICLES_LOAD_CANCELED,
  payload: undefined,
});

export type ArticleMarkedReadAction = Action<
  ServerActionTypes.ARTICLE_MARKED_READ,
  { articleId: string }
>;
export const articleMarkedRead = (
  articleId: string
): ArticleMarkedReadAction => ({
  type: ServerActionTypes.ARTICLE_MARKED_READ,
  payload: { articleId },
});
