export interface ServerBoundActionMeta {
  shouldBeQueued?: boolean;
}

export interface NetworkActionMeta {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  queuedActions: Action<any, any, any>[];
}

export type MetaType = ServerBoundActionMeta | NetworkActionMeta | undefined;

export function isNetworkActionMeta(meta: MetaType): meta is NetworkActionMeta {
  return (meta as NetworkActionMeta).queuedActions !== undefined;
}

export function isServerBoundActionMeta(
  meta: MetaType
): meta is ServerBoundActionMeta {
  return (meta as ServerBoundActionMeta).shouldBeQueued !== undefined;
}

export interface Action<
  Type extends string,
  PayloadType = undefined,
  Meta extends MetaType = undefined
> {
  type: Type;
  payload: PayloadType;
  meta?: Meta;
}
