import {
  applyMiddleware,
  compose,
  combineReducers,
  createStore,
  Store,
} from 'redux';
import { createBrowserHistory } from 'history';
import { logger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import localforage from 'localforage';
import {
  connectRouter,
  routerMiddleware,
  RouterState,
} from 'connected-react-router';
import {
  networkCameOnline,
  networkWentOffline,
} from './actions/networkActions';
import { watchLoginActions } from './sagas/sessionSagas';
import { watchFeedActions } from './sagas/feedsSagas';
import { watchNetworkActions } from './sagas/networkSagas';
import { userReducer, UserState } from './reducers/userReducer';
import { feedsReducer, FeedsState } from './reducers/feedsReducer';
import { headlinesReducer, HeadlinesState } from './reducers/headlinesReducer';
import { articlesReducer, ArticlesState } from './reducers/articlesReducer';
import {
  categoriesReducer,
  CategoriesState,
} from './reducers/categoriesReducer';
import { offlineReducer, OfflineState } from './reducers/offlineReducer';

export const history = createBrowserHistory();
export const sagaMiddleware = createSagaMiddleware();

export interface CachedEntities<EntityType> {
  [id: string]: EntityType;
}

export interface EntityCache<EntityType> {
  entities: CachedEntities<EntityType>;
  isLoading: boolean;
}

export interface AppState {
  articles: ArticlesState;
  categories: CategoriesState;
  feeds: FeedsState;
  headlines: HeadlinesState;
  offline: OfflineState;
  router: RouterState;
  user: UserState;
}

const rootReducer = combineReducers({
  articles: articlesReducer,
  categories: categoriesReducer,
  feeds: feedsReducer,
  headlines: headlinesReducer,
  offline: offlineReducer,
  router: connectRouter(history),
  user: userReducer,
});

const stateReconciler = (
  inboundState: AppState,
  originalState: AppState,
  reducedState: AppState
): AppState => {
  if (!inboundState) {
    return reducedState;
  }
  return {
    ...reducedState,
    ...inboundState,
    offline: {
      ...reducedState.offline,
      ...inboundState.offline,
      networkStatus: reducedState.offline.networkStatus,
    },
  };
};

const persistConfig = {
  key: 'root',
  storage: localforage,
  stateReconciler,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [
  applyMiddleware(sagaMiddleware),
  applyMiddleware(routerMiddleware(history)),
];

if (process.env.NODE_ENV === 'development') {
  middlewares.push(applyMiddleware(logger));
}

export const store = createStore(persistedReducer, compose(...middlewares));

export const persistor = persistStore(store);

declare global {
  interface Window {
    store: Store;
  }
}

window.store = store;

window.addEventListener('online', () => store.dispatch(networkCameOnline()));
window.addEventListener('offline', () => store.dispatch(networkWentOffline()));

sagaMiddleware.run(watchLoginActions);
sagaMiddleware.run(watchFeedActions);
sagaMiddleware.run(watchNetworkActions);
