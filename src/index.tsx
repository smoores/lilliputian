import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './components/App/App';
import * as serviceWorker from './serviceWorker';

if (process.env.NODE_ENV === 'development') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const eruda = require('eruda');
  eruda.init();
}
ReactDOM.render(<App />, document.getElementById('root'));

if (process.env.NODE_ENV !== 'development') {
  serviceWorker.register();
}
