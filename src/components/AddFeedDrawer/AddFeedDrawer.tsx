import React, { useMemo, useState, FormEvent } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import * as styles from './styled';
import { useDrawer } from '../../hooks/useDrawer';
import { CloseButton } from '../CloseButton/CloseButton';
import { useShallowEqualSelector } from '../../hooks/redux';
import { useDispatch } from 'react-redux';
import { subscribeToFeedSubmitted } from '../../store/actions/userActions';
import { DRAWER_TOP_PERCENT } from '../styling';

interface AddFeedDrawerProps {
  isOpen: boolean;
}

const InnerAddFeedDrawer = (
  { isOpen }: AddFeedDrawerProps,
  ref: React.Ref<HTMLDivElement>
) => {
  const [feedUrl, setFeedUrl] = useState('');
  const [category, setCategory] = useState('0');
  const { onClose } = useDrawer(isOpen);
  const categories = useShallowEqualSelector(
    (state) => state.categories.entities
  );
  const categoriesList = useMemo(() => Object.values(categories), [categories]);
  const dispatch = useDispatch();
  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch(subscribeToFeedSubmitted(feedUrl, category));
  };

  return (
    <AnimatePresence>
      {isOpen && (
        <motion.div
          animate={{ top: `${DRAWER_TOP_PERCENT}%` }}
          initial={{ top: '100%' }}
          exit={{ top: '100%' }}
          transition={{ duration: 0.25, type: 'tween', ease: 'easeOut' }}
          ref={ref}
          className={styles.drawerClass}
        >
          <CloseButton onClose={onClose} />
          Add Feed
          <form onSubmit={onSubmit}>
            <fieldset>
              <label htmlFor="url">URL</label>
              <input
                type="text"
                id="url"
                value={feedUrl}
                onChange={(e) => setFeedUrl(e.target.value)}
              ></input>
            </fieldset>
            <fieldset>
              <label htmlFor="category">Category</label>
              <select
                id="category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value="0">Uncategorized</option>
                {categoriesList.map((category) => (
                  <option key={category.id} value={category.id}>
                    {category.name}
                  </option>
                ))}
              </select>
            </fieldset>
            <button>Subscribe</button>
          </form>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export const AddFeedDrawer = React.forwardRef(InnerAddFeedDrawer);
