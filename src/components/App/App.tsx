import React, { useState, useLayoutEffect } from 'react';
import { Provider } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store, history } from '../../store';
import { LoginPage } from '../../pages/LoginPage/LoginPage';
import { LoginRedirect } from '../LoginRedirect/LoginRedirect';
import { HomePage } from '../../pages/HomePage/HomePage';
import { FeedPage } from '../../pages/FeedPage/FeedPage';
import './styled';
import { DrawerContext } from '../../contexts/DrawerContext';

export const App = () => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  useLayoutEffect(() => {
    if (isDrawerOpen) {
      document.body.classList.add('black-background');
    } else {
      document.body.classList.remove('black-background');
    }
  }, [isDrawerOpen]);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ConnectedRouter history={history}>
          <DrawerContext.Provider value={{ isDrawerOpen, setIsDrawerOpen }}>
            <Switch>
              <LoginRedirect exact path="/" component={HomePage} />
              <Route path="/login" component={LoginPage} />
              <LoginRedirect path="/settings" component={HomePage} />
              <LoginRedirect
                path="/:feedType/:feedId/article/:articleId"
                component={FeedPage}
              />
              <LoginRedirect path="/:feedType/:feedId" component={FeedPage} />
              <Route
                path="/"
                render={(props) => <Redirect {...props} to="/" />}
              />
            </Switch>
          </DrawerContext.Provider>
        </ConnectedRouter>
      </PersistGate>
    </Provider>
  );
};
