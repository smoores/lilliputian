import { injectGlobal } from 'emotion';
import {
  BACKGROUND_COLOR,
  TEXT_COLOR,
  DARK_MODE_BACKGROUND_COLOR,
  DARK_MODE_TEXT_COLOR,
} from '../styling';

injectGlobal`
  html {
    box-sizing: border-box;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    font-size: 16px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body.black-background {
    background-color: black;
  }

  h1 {
    padding-top: 12px;
    margin-top: 0;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  button {
    color: inherit;
  }

  path {
    fill: currentColor;
  }

  background: ${BACKGROUND_COLOR};
  color: ${TEXT_COLOR};
  @media only screen and (prefers-color-scheme: dark) {
    body {
      background: ${DARK_MODE_BACKGROUND_COLOR};
      color: ${DARK_MODE_TEXT_COLOR};
    }
  }
`;
