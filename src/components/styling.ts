import { css } from 'emotion';

export const NEUTRAL_GRAY = '#AAA';
export const PRIMARY_BLUE = '#1049A9';
export const LIGHT_GRAY = '#EEE';
export const BACKGROUND_COLOR = 'white';
export const TEXT_COLOR = 'black';

export const DARK_MODE_BLUE = '#5079E9';
export const DARK_MODE_BACKGROUND_COLOR = 'black';
export const DARK_MODE_TEXT_COLOR = 'white';

export const cleanListClass = css`
  list-style: none;
  padding: 0;
`;

export const headerClass = css`
  text-align: center;

  & > h1 {
    width: calc(100% - 128px);
    margin: auto;
    padding-top: 16px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 1.2rem;
  }
`;

export const DRAWER_TOP_PERCENT = 5;

export const drawerClass = css`
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  top: ${DRAWER_TOP_PERCENT}%;
  left: 0;
  height: ${100 - DRAWER_TOP_PERCENT}%;
  width: 100%;
  padding: 24px;
  border-radius: 8px 8px 0 0;
  background: ${BACKGROUND_COLOR};
  opacity: 1;

  a {
    color: ${PRIMARY_BLUE};
    font-weight: 600;
  }

  @media only screen and (prefers-color-scheme: dark) {
    background: ${DARK_MODE_BACKGROUND_COLOR};
    a {
      color: ${DARK_MODE_BLUE};
    }
  }
`;

export const pageClass = css`
  height: 100%;
  background: ${BACKGROUND_COLOR};

  @media only screen and (prefers-color-scheme: dark) {
    background: ${DARK_MODE_BACKGROUND_COLOR};
  }
`;

export const truncatedClass = css`
  overflow: hidden;
  margin: auto;
  opacity: 0.6;
  transform: translateY(-5%) scale(0.9);
`;
