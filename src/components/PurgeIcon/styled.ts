import { css } from 'emotion';

export const purgeIconClass = css`
  position: absolute;
  top: 12px;
  right: 16px;
  background: none;
  border: none;
  padding: 0;
`;
