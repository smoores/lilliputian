import React from 'react';
import Purge from '@material-ui/icons/Cached';
import * as styles from './styled';
import { persistor } from '../../store';

const purge = async () => {
  await persistor.purge();
  window.location.reload();
};

export const PurgeIcon = () => (
  <button className={styles.purgeIconClass} onClick={purge}>
    <Purge />
  </button>
);
