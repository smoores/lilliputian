import React, { useEffect } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { highlightBlock } from 'highlight.js';
import 'highlight.js/styles/darkula.css';
import * as styles from './styled';
import { CloseButton } from './../CloseButton/CloseButton';
import { useShallowEqualSelector } from '../../hooks/redux';
import { useDispatch } from 'react-redux';
import { articleDrawerOpened } from '../../store/actions/userActions';
import { useDrawer } from '../../hooks/useDrawer';
import { DRAWER_TOP_PERCENT } from '../styling';

export interface ArticleDrawerProps {
  articleId?: string;
}

const InnerArticleDrawer = (
  { articleId }: ArticleDrawerProps,
  ref: React.Ref<HTMLElement>
) => {
  const article = useShallowEqualSelector(
    (state) =>
      articleId &&
      state.articles.entities[articleId] &&
      state.articles.entities[articleId].entity
  );
  const isLoading = useShallowEqualSelector(
    (state) =>
      articleId &&
      state.articles.entities[articleId] &&
      state.articles.entities[articleId].isLoading
  );
  const feed = useShallowEqualSelector(
    (state) => article && state.feeds.entities[article.feed_id]
  );

  const dispatch = useDispatch();
  const onLoad = () => {
    if (!isLoading && articleId) {
      dispatch(articleDrawerOpened(articleId));
    }
  };
  const { onClose } = useDrawer(!!articleId);

  useEffect(onLoad, []);
  useEffect(() => {
    document.querySelectorAll('pre code').forEach((block) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const parent = block.parentElement!;
      // CSS-Tricks adds the language of the code in the code block
      // as a `rel` attribute on the pre block. Weird, but convenient.
      const relAttr = parent.attributes.getNamedItem('rel');
      if (relAttr) {
        parent.classList.add(`language-${relAttr.value.toLowerCase()}`);
      }
      highlightBlock(block);
    });
  }, []);

  return (
    <AnimatePresence>
      {articleId && (
        <motion.article
          animate={{ top: `${DRAWER_TOP_PERCENT}%` }}
          initial={{ top: '100%' }}
          exit={{ top: '100%' }}
          transition={{ duration: 0.25, type: 'tween', ease: 'easeOut' }}
          className={styles.drawerClass}
          ref={ref}
        >
          {article && (
            <React.Fragment>
              <CloseButton onClose={onClose} />
              <header>
                <h3>{article.title}</h3>
              </header>
              <main className={styles.articleContentClass}>
                <header className={styles.feedClass}>
                  <div className={styles.imgContainerClass}>
                    <img
                      alt=""
                      src={
                        feed &&
                        `${process.env.REACT_APP_TTRSS_ENDPOINT}/${feed.icon}`
                      }
                    />
                  </div>
                  <p className={styles.feedTextClass}>{feed && feed.name}</p>
                </header>
                <section
                  className={styles.articleBodyClass}
                  dangerouslySetInnerHTML={{ __html: article.content }}
                />
              </main>
              <a href={article.link}>Full article</a>
            </React.Fragment>
          )}
        </motion.article>
      )}
    </AnimatePresence>
  );
};

export const ArticleDrawer = React.forwardRef(InnerArticleDrawer);
