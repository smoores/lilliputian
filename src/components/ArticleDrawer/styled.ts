import { css } from 'emotion';
import {
  imgContainerClass,
  headlineTextClass,
} from '../../pages/FeedPage/styled';
import { drawerClass } from '../styling';

export { imgContainerClass, drawerClass };

export const articleContentClass = css`
  flex-basis: 75%;
  flex-shrink: 1;
  overflow: scroll;
  -webkit-overflow-scrolling: touch;
  section img {
    max-width: 80%;
  }
`;

export const articleBodyClass = css`
  overflow-x: hidden;
  pre,
  code {
    overflow-x: auto;
  }
`;

export const feedClass = css`
  display: flex;
  align-items: center;
  width: calc(100% - 16px);
`;

export const feedTextClass = css`
  ${headlineTextClass}
  padding-left: 2rem;
`;
