import React from 'react';
import { RouteComponentProps, Route, Redirect, RouteProps } from 'react-router';
import { useShallowEqualSelector } from '../../hooks/redux';

interface LoginRedirectProps extends RouteProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: React.ComponentType<RouteComponentProps<any>>;
}

export const LoginRedirect = ({
  component: Component,
  ...rest
}: LoginRedirectProps) => {
  const loggedIn = useShallowEqualSelector((state) => !!state.user.sid);
  return (
    <Route
      {...rest}
      render={(props) =>
        loggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
