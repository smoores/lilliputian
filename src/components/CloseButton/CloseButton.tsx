import React from 'react';
import * as styles from './styled';

interface CloseButtonProps {
  onClose: () => void;
}

export const CloseButton = ({ onClose }: CloseButtonProps) => {
  return (
    <button
      className={styles.closeIconClass}
      onClick={onClose}
      // onKeyPress={(e) => e.key === 'Enter' && onClose()}
    >
      &#x2716;
    </button>
  );
};
