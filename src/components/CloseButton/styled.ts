import { css } from 'emotion';

export const closeIconClass = css`
  position: absolute;
  top: 16px;
  right: 16px;
  border: none;
  background: none;
  font-size: 1rem;
  padding: 0;
  margin: 0;
`;
