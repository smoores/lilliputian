import { css } from 'emotion';
import {
  NEUTRAL_GRAY,
  PRIMARY_BLUE,
  DARK_MODE_BLUE,
} from '../../components/styling';

export const formClass = css`
  width: 70%;
  margin-right: auto;
  margin-left: auto;

  & > input {
    display: block;
    width: calc(100% - 16px);
    margin: 0 0 16px;
    border-radius: 2px;
    border-color: ${NEUTRAL_GRAY};
    font-size: 1rem;
  }
`;

export const headerClass = css`
  text-align: center;
`;

export const buttonClass = css`
  width: 100%;
  padding: 8px;
  border-radius: 2px;
  border: none;
  background-color: ${PRIMARY_BLUE};
  color: white;
  font-size: 1rem;

  @media only screen and (prefers-color-scheme: dark) {
    background-color: ${DARK_MODE_BLUE};
  }
`;

export const pageClass = css`
  width: 100%;
  height: 100%;
`;
