import React, { useState, useCallback } from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import * as styles from './styled';
import { useShallowEqualSelector } from '../../hooks/redux';
import { useDispatch } from 'react-redux';
import { loginClicked } from '../../store/actions/userActions';

export const LoginPage = ({ location }: RouteComponentProps) => {
  const loggedIn = useShallowEqualSelector((state) => !!state.user.sid);
  const cachedUsername = useShallowEqualSelector(
    (state) => state.user.userName
  );
  const dispatch = useDispatch();
  const onClick = useCallback(
    (username, password) => dispatch(loginClicked(username, password)),
    [dispatch]
  );

  const [username, setUsername] = useState(cachedUsername || '');
  const [password, setPassword] = useState('');
  const { from } = location.state || { from: { pathname: '/' } };
  if (loggedIn) {
    return <Redirect to={from} />;
  }
  return (
    <div className={styles.pageClass}>
      <form className={styles.formClass} onSubmit={(e) => e.preventDefault()}>
        <h2 className={styles.headerClass}>
          Log in with your TT-RSS credentials
        </h2>
        <input
          name="username"
          autoComplete="username"
          placeholder="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          name="password"
          type="password"
          autoComplete="current-password"
          placeholder="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button
          className={styles.buttonClass}
          type="submit"
          onClick={() => onClick(username, password)}
        >
          Login
        </button>
      </form>
    </div>
  );
};
