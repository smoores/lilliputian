import { css } from 'emotion';
import {
  cleanListClass,
  headerClass,
  NEUTRAL_GRAY,
  LIGHT_GRAY,
  pageClass,
  truncatedClass,
} from '../../components/styling';

export { cleanListClass, headerClass, pageClass, truncatedClass };

export const homeIconClass = css`
  position: absolute;
  top: 10px;
  left: 16px;
  width: 40px;
  height: 40px;
`;

export const headlineClass = css`
  display: flex;
  align-items: center;
  width: calc(100% - 16px);
  padding: 8px;
  border-top: 1px solid ${LIGHT_GRAY};
  font-size: 0.9rem;
`;

export const headlineMetaClass = css`
  display: inline-flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  height: 56px;
  width: calc(100% - 56px);
  margin-left: 8px;
  & > p {
    margin: 0;
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const updatedClass = css`
  text-transform: uppercase;
  font-size: 0.7rem;
  color: ${NEUTRAL_GRAY};
  font-weight: 600;
`;

export const authorClass = css`
  color: ${NEUTRAL_GRAY};
  font-size: 0.7rem;
`;

export const tagClass = css`
  margin-left: 16px;
  font-size: 0.6rem;
  border: 1px solid ${NEUTRAL_GRAY};
  border-radius: 2px;
  padding: 0 4px;
  color: ${NEUTRAL_GRAY};
`;

export const headlineTextClass = css`
  font-weight: 600;
`;

export const imgContainerClass = css`
  display: inline-block;
  height: 56px;
  width: 56px;
  flex-shrink: 0;
  border-radius: 8px;
  overflow: hidden;

  & > img {
    margin-left: -8px;
    margin-top: -8px;
    height: 72px;
    width: 72px;
  }
`;
