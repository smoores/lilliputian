import React, { useEffect, useMemo, useContext } from 'react';
import { cx } from 'emotion';
import { Link, RouteComponentProps } from 'react-router-dom';
import moment from 'moment';
import { ArticleDrawer } from '../../components/ArticleDrawer/ArticleDrawer';
import { Headline } from '../../store/reducers/headlinesReducer';
import { Feed } from '../../store/reducers/feedsReducer';
import * as styles from './styled';
import { PurgeIcon } from '../../components/PurgeIcon/PurgeIcon';
import { Logo } from '../../icons/Logo';
import { useShallowEqualSelector } from '../../hooks/redux';
import { useDispatch } from 'react-redux';
import { feedPageLoaded } from '../../store/actions/userActions';
import { Category } from '../../store/reducers/categoriesReducer';
import { DrawerContext } from '../../contexts/DrawerContext';

export type FeedPageProps = RouteComponentProps<{
  feedId: string;
  feedType: string;
  articleId?: string;
}>;

export interface ArticleLinkProps {
  feedType?: string;
  feed: Feed | Category;
  headline: Headline;
  icon?: string;
}

function createReadableDayString(seconds: number) {
  const today = moment();
  const updated = moment.unix(seconds);
  if (today.diff(updated, 'days') < 7) {
    return updated.format('dddd');
  }
  return updated.format('YYYY/MM/DD');
}

const ArticleLink = ({ headline, feedType, feed, icon }: ArticleLinkProps) => (
  <Link key={headline.id} to={`/${feedType}/${feed.id}/article/${headline.id}`}>
    <li className={styles.headlineClass}>
      <span className={styles.imgContainerClass}>
        <img alt="" src={icon} />
      </span>
      <span className={styles.headlineMetaClass}>
        <p className={styles.updatedClass}>
          {createReadableDayString(headline.updated)}
        </p>
        <p
          className={cx({
            [styles.headlineTextClass]: headline.unread,
          })}
        >
          {headline.title}
        </p>
        <p>
          <span className={styles.authorClass}>{headline.author}</span>
          {headline.tags
            .filter((tag) => tag !== '')
            .map((tag) => (
              <span key={tag} className={styles.tagClass}>
                {tag}
              </span>
            ))}
        </p>
      </span>
    </li>
  </Link>
);

export const FeedPage = ({
  match: {
    params: { feedId, feedType, articleId },
  },
}: FeedPageProps) => {
  const feed = useShallowEqualSelector((state) => {
    const cache = feedType === 'cat' ? state.categories : state.feeds;
    return cache.entities[feedId];
  });
  const feeds = useShallowEqualSelector((state) => state.feeds.entities);
  const allHeadlines = useShallowEqualSelector(
    (state) => state.headlines.entities
  );
  const headlines = useMemo(
    () =>
      feed &&
      feed.headlines &&
      feed.headlines
        .map((headlineId) => allHeadlines[headlineId])
        .filter((headline) => headline !== undefined),
    [allHeadlines, feed]
  );
  const isLoading = useShallowEqualSelector(
    (state) => !!state.feeds.isLoading && !!state.headlines.isLoading
  );
  const { isDrawerOpen } = useContext(DrawerContext);

  const dispatch = useDispatch();

  useEffect(() => {
    if (!isLoading) {
      dispatch(feedPageLoaded(feedId, feedType === 'cat'));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div
        className={cx(styles.pageClass, {
          [styles.truncatedClass]: isDrawerOpen,
        })}
      >
        <header className={styles.headerClass}>
          <Link to="/">
            <Logo className={styles.homeIconClass} />
          </Link>
          <h1>{feed ? feed.name : 'Loading...'}</h1>
          <PurgeIcon />
        </header>
        <main>
          {feed && headlines && (
            <ul className={styles.cleanListClass}>
              {headlines.map((headline) => (
                <ArticleLink
                  key={headline.id}
                  feed={feed}
                  feedType={feedType}
                  headline={headline}
                  icon={
                    feeds[headline.feed_id] &&
                    `${process.env.REACT_APP_TTRSS_ENDPOINT}/${
                      feeds[headline.feed_id].icon
                    }`
                  }
                />
              ))}
            </ul>
          )}
        </main>
      </div>
      <ArticleDrawer key="articleDrawer" articleId={articleId} />
    </>
  );
};
