import React, { useEffect, useMemo, useContext } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import archive from '@material-ui/icons/Archive';
import inbox from '@material-ui/icons/Inbox';
import restore from '@material-ui/icons/Restore';
import rssFeed from '@material-ui/icons/RssFeed';
import star from '@material-ui/icons/Star';
import whatshot from '@material-ui/icons/Whatshot';
import { Feed } from '../../store/reducers/feedsReducer';
import { PurgeIcon } from '../../components/PurgeIcon/PurgeIcon';
import * as styles from './styled';
import { useDispatch } from 'react-redux';
import { useShallowEqualSelector } from '../../hooks/redux';
import { homePageLoaded } from '../../store/actions/userActions';
import { AddFeedDrawer as SubscribeToFeedDrawer } from '../../components/AddFeedDrawer/AddFeedDrawer';
import { cx } from 'emotion';
import { DrawerContext } from '../../contexts/DrawerContext';

const materialIcons = {
  archive,
  inbox,
  restore,
  // eslint-disable-next-line @typescript-eslint/camelcase
  rss_feed: rssFeed,
  star,
  whatshot,
};

export interface FeedLinkProps {
  feed: Feed;
}

export const FeedLink = ({ feed }: FeedLinkProps) => (
  <Link key={feed.id} to={`/feed/${feed.id}`}>
    <li className={styles.feedClass} key={feed.id}>
      {feed.icon &&
        (feed.icon in materialIcons ? (
          React.createElement(
            materialIcons[feed.icon as keyof typeof materialIcons]
          )
        ) : (
          <img src={`${process.env.REACT_APP_TTRSS_ENDPOINT}/${feed.icon}`} />
        ))}
      {feed.name}
    </li>
  </Link>
);
export const HomePage = ({ location }: RouteComponentProps) => {
  const { isDrawerOpen } = useContext(DrawerContext);
  const dispatch = useDispatch();
  const isLoading = useShallowEqualSelector((state) => state.feeds.isLoading);
  useEffect(() => {
    if (!isLoading) {
      dispatch(homePageLoaded());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const categories = useShallowEqualSelector(
    (state) => state.categories.entities
  );
  const categoriesList = useMemo(() => Object.values(categories), [categories]);
  const feeds = useShallowEqualSelector((state) => state.feeds.entities);

  return (
    <>
      <div
        className={cx(styles.pageClass, {
          [styles.truncatedClass]: isDrawerOpen,
        })}
      >
        <header className={styles.headerClass}>
          <h1>Feeds</h1>
          <PurgeIcon />
        </header>
        <main>
          <ul className={styles.cleanListClass}>
            {categoriesList.map((category) => (
              <li className={styles.categoryClass} key={category.id}>
                <Link to={`/cat/${category.id}/`}>
                  <h2>{category.name}</h2>
                </Link>
                <ul className={styles.cleanListClass}>
                  {category.feeds.map((feedId) => {
                    return <FeedLink key={feedId} feed={feeds[feedId]} />;
                  })}
                </ul>
              </li>
            ))}
            <li className={styles.categoryClass}>
              <h2>Settings</h2>
              <ul className={styles.cleanListClass}>
                <li className={styles.feedClass}>
                  <Link to={'/settings/subscribe'}>Add Feed</Link>
                </li>
              </ul>
            </li>
          </ul>
        </main>
      </div>
      <SubscribeToFeedDrawer
        isOpen={location.pathname === '/settings/subscribe'}
      />
    </>
  );
};
