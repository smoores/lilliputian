import { css } from 'emotion';
import {
  LIGHT_GRAY,
  cleanListClass,
  headerClass,
  pageClass,
  truncatedClass,
} from '../../components/styling';

export { cleanListClass, headerClass, pageClass, truncatedClass };

export const categoryClass = css`
  margin-top: 24px;
  & h2 {
    padding-left: 24px;
    font-weight: bold;
    font-size: 1rem;
  }
`;

export const feedClass = css`
  display: flex;
  align-items: center;
  padding: 24px 0 24px 8px;
  margin-left: 24px;
  border-bottom: 1px solid ${LIGHT_GRAY};
  font-weight: normal;

  & > img,
  & > svg {
    width: 32px;
    padding-right: 16px;
  }
`;
