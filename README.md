# Lilliputian

Lilliputian is a [Progressive Web App](https://developers.google.com/web/progressive-web-apps/) frontend for [Tiny Tiny RSS](https://tt-rss.org).

![A side-by-side of two pages in Lilliputian](screenshot.png)*Supports dark mode (left) and light mode (right)*

## Installation

Lilliputian is just a frontend. You must have an existing Tiny Tiny RSS installation for it to work! Read the [Tiny Tiny RSS Installation Guide](https://tt-rss.org/wiki/InstallationNotes) for more information on setting up your own Tiny Tiny RSS instance.

This project is built in React and Typescript, and uses `REACT_APP_` prefixed environment variables to inject configuration at build time. This means that the easiest way to configure and run the project is to build from source. Luckily, this is easy to do!

1. Clone this repository
2. Run `yarn` to install dependencies
3. Add a `.env` file at the top level, setting a variable called `REACT_APP_TTRSS_ENDPOINT` to your Tiny Tiny RSS endpoint. For example:

```
REACT_APP_TTRSS_ENDPOINT=https://ttrss.mydomain.com
```

4. Run `yarn build` to compile the project (alternatively, you can skip step 3 and run `REACT_APP_TTRSS_ENDPOINT=<your-ttrss-endpoint> yarn build`)
5. Serve from the `build` directory using your favorite reverse proxy or static site server

### Adding CORS support to TTRSS

Most likely, your Tiny Tiny RSS instance is blocking cross origin requests (this is the default). If you serve your instance through a reverse proxy you can add CORS headers there. Here's an example for caddy:

```
ttrss.example.com {
  cors /api https://lilliputian.example.com
  header /api Access-Control-Allow-Methods POST, OPTIONS
  header /api Access-Control-Allow-Headers Content-Type
  ...
}
```

Otherwise, you can add these headers directly to your ttrss instance. Add the following to `ttrss/config.php`, swapping out `lilliputian.example.com` for your actual domain (e.g. `localhost:5000` if you're just serving locally with `serve`):

```php
if (strpos($_SERVER["SCRIPT_NAME"], 'api/index.php') !== false) {
  header("Access-Control-Allow-Origin: lilliputian.example.com");
  header("Access-Control-Allow-Methods: POST, OPTIONS");
  header("Access-Control-Allow-Headers: Content-Type");
}
```

## Serving your Lilliputian Instance

If you only need Lilliputian to be available on your local network, you can use `serve` to serve your static bundle. Simply run `yarn global add serve` to install the `serve` binary, and then run `serve -s build -p 8114` after building to serve your Lilliputian instance on port `8114`.

### Using a reverse proxy

If you already have a reverse proxy like [nginx](https://nginx.org/en/docs/beginners_guide.html) or [caddy](https://caddyserver.com/v1/) set up, it should be straightforward to serve your static files from the `build/` directory. Here's an example caddy server config:

```
lilliputian.example.com {
  header / Strict-Transport-Security "max-age=15768000;"
  root /path/to/your/repo/build
}
```

## Development

Lilliputian was created with create-react-app, and then ejected. None of the original create-react-app scripts have been modified; the codebase is still largely the same as any other create-react-app project.

Package management is done with [yarn](https://classic.yarnpkg.com/en/docs/install#mac-stable).

Useful development tools:
  * Install dependencies by running `yarn`.
  * Start the development server (with hot reloading) with `yarn start`.
  * Compile a production build with `yarn build`.
  * Start the interactive test runner with `yarn test` (at the time of writing, we don't have any tests yet!)
  * Upgrade dependencies with `yarn upgrade` (it's not enough to update the `package.json` manually, don't do it!)
